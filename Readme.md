
# User Management System

This project is a user management system that allows administrators to perform CRUD (Create, Read, Update, Delete) operations on user accounts.


## Features

- **User Listing**: View a list of all users by various means as mentioned with pagination support.
- **User Creation**: Create new user accounts with specified details.
- **User Editing**: Update existing user account details.
- **User Deletion**: Delete user accounts from the system.

## Technologies used

- **Language** : Java

- **Database**: MySQL

- **Framework**: Spring Boot, Spring Data JPA, Spring Security, Spring OAuth2

- **Other Tools**: MapStruct, Lombok

- **testing Tools**: Junit, Mokito

- **Application for testing APIs**: Postman , Swagger

## Setup project locally

Firstly, clone the project to your machine

```bash
  git clone https://gitlab.com/suminshakya/user-management-system.git
```
Then create a database named user_management_system in your MySQL database

```bash
  CREATE DATABASE user_management_system;
```

Then go to the project directory

```bash
  cd user_management_system
```
Then install gradle dependencies used in project

```bash
  gradle clean build
```
Start the server with available username and password of MySQL DB

```bash
    Add your userrname and password of mysql and then run command
    
    gradle bootRun
```

## API Reference
### For the api documentation and access the secured API
- **Open a web browser and go to [http://localhost:8080/swagger-ui/index.html]**
- **Also I have provided the post man collection in the folder called post-collection located in project resource folder**

### Note : Use the login API to authenticate and use the token in the response to access the secured api by clicking Authorize button on Swagger