package com.kotuko.ums;

import com.kotuko.ums.config.RsaKeyConfigProperties;
import com.kotuko.ums.model.User;
import com.kotuko.ums.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.time.LocalDate;
import java.util.Optional;

@SpringBootApplication
@EnableConfigurationProperties(RsaKeyConfigProperties.class)
@RequiredArgsConstructor
public class UMSApplication implements CommandLineRunner {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    public static void main(String[] args) {
        SpringApplication.run(UMSApplication.class, args);
    }

    @Override
    public void run(String... args) throws Exception {
        User user = User.builder()
                .firstName("admin")
                .lastName("admin")
                .dateOfBirth(LocalDate.now().minusDays(4))
                .email("admin@admin.com")
                .username("admin")
                .password(passwordEncoder.encode("admin"))
                .build();
        Optional<User> existedUserName = userRepository.findByUsernameOrEmail("admin", "admin@admin.com");
        if (!existedUserName.isPresent()) {
            userRepository.save(user);
        }

    }
}
