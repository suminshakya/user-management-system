package com.kotuko.ums.controller;

import com.kotuko.ums.dto.ErrorMessage;
import com.kotuko.ums.dto.ExceptionResponseDTO;
import com.kotuko.ums.exception.DataNotFoundException;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import java.io.IOException;
import java.util.ArrayList;


@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public ResponseEntity<Object> constraintViolationException(final MethodArgumentNotValidException ex, final HttpServletRequest httpServletRequest) {
        ErrorMessage errorResponse =  ErrorMessage.builder().status(HttpStatus.BAD_REQUEST)
                .apiPath(httpServletRequest.getRequestURI()).errors(new ArrayList<>())
                .build();

        ex.getBindingResult().getAllErrors().forEach(error -> errorResponse
                .getErrors().add(error.getDefaultMessage()));
        return new ResponseEntity<>(errorResponse, errorResponse.getStatus());
    }

    @ExceptionHandler(DataNotFoundException.class)
    public ResponseEntity<ExceptionResponseDTO> emptyListException(final DataNotFoundException ex, final HttpServletRequest request) {
        ExceptionResponseDTO error = new ExceptionResponseDTO(ex.getLocalizedMessage(), request.getRequestURI());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(IOException.class)
    public ResponseEntity<ExceptionResponseDTO> emptyListException(final IOException ex, final HttpServletRequest request) {
        ExceptionResponseDTO error = new ExceptionResponseDTO(ex.getLocalizedMessage(), request.getRequestURI());
        return new ResponseEntity<>(error, HttpStatus.BAD_REQUEST);
    }
}
