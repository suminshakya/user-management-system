package com.kotuko.ums.controller;

import com.kotuko.ums.dto.AuthUserDTO;
import com.kotuko.ums.service.LoginService;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import static com.kotuko.ums.util.MessageConstraints.APIConstant.*;

@Tag(name = "Login Controller", description = "Operations related to the authentication")
@RestController
@RequiredArgsConstructor
@RequestMapping(URL_CONSTANT + LOGIN)
public class LoginController {

    private final LoginService loginService;

    @PostMapping
    public ResponseEntity<AuthUserDTO.Response> login(@RequestBody AuthUserDTO.LoginRequest userLogin)  {
        return new ResponseEntity<>(loginService.authenticate(userLogin), HttpStatus.OK);
    }
}
