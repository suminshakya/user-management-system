package com.kotuko.ums.controller;

import com.kotuko.ums.dto.MessageResponseDTO;
import com.kotuko.ums.dto.UserDTO;
import com.kotuko.ums.marker.OnCreate;
import com.kotuko.ums.marker.OnUpdate;
import com.kotuko.ums.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import lombok.AllArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static com.kotuko.ums.util.MessageConstraints.APIConstant.*;
import static com.kotuko.ums.util.MessageConstraints.SuccessfulMessage.*;


@Tag(name = "User Controller", description = "Operations related to the user actions")
@RestController
@RequestMapping(URL_CONSTANT + USERS)
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @PostMapping
    @Operation(
            summary = "Create User",
            description = "Save User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful inserted")
    })
    public ResponseEntity<MessageResponseDTO> add(@Validated(OnCreate.class) @RequestBody UserDTO user) {
        userService.add(user);
        return new ResponseEntity<>(new MessageResponseDTO(USER_ADDED), HttpStatus.OK);
    }

    @PutMapping(ID_PATH)
    @Operation(summary = "Update User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful updated")
    })
    public ResponseEntity<MessageResponseDTO> update(@PathVariable Long id,@Validated(OnUpdate.class) @RequestBody UserDTO user) {
        userService.update(id, user);
        return new ResponseEntity<>(new MessageResponseDTO(USER_UPDATED), HttpStatus.OK);
    }

    @DeleteMapping(ID_PATH)
    @Operation(summary = "Delete User")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful deleted")
    })
    public ResponseEntity<MessageResponseDTO> delete(@PathVariable Long id) {
        userService.delete(id);
        return new ResponseEntity<>(new MessageResponseDTO(USER_DELETED), HttpStatus.OK);
    }

    @GetMapping(FIRSTNAME + FIRST_NAME_PATH)
    @Operation(summary = "Fetch user having same first name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful retrieved")
    })
    public ResponseEntity<List<UserDTO>> getAllByFirstName(@PathVariable String firstName,
                                                           @PageableDefault(size = 10, sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        return new ResponseEntity<>(userService.getByFirstName(firstName.trim(), pageable), HttpStatus.OK);
    }

    @GetMapping(EMAIL + EMAIL_PATH)
    @Operation(summary = "Fetch user having same email")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful retrieved")
    })
    public ResponseEntity<UserDTO> getAllByEmail(@PathVariable String email) {
        return new ResponseEntity<>(userService.getByEmail(email.trim()), HttpStatus.OK);
    }

    @GetMapping(LASTNAME + LAST_NAME_PATH)
    @Operation(summary = "Fetch user having same last name")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful retrieved")
    })
    public ResponseEntity<List<UserDTO>> getAllByLastName(@PathVariable String lastName,
                                                          @PageableDefault(size = 10, sort = "id", direction = Sort.Direction.DESC) Pageable pageable) {
        return new ResponseEntity<>(userService.getByLastName(lastName.trim(), pageable), HttpStatus.OK);
    }

    @GetMapping(USERNAME_PATH)
    @Operation(summary = "Fetch user having same username")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "successful retrieved")
    })
    public ResponseEntity<UserDTO> getAllByUsername(@PathVariable String username) {
        return new ResponseEntity<>(userService.getByUsername(username.trim()), HttpStatus.OK);
    }
}
