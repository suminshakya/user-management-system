package com.kotuko.ums.dto;

public class AuthUserDTO {
    public record LoginRequest(String username, String password) {
    }

    public record Response(String message, String token) {
    }
}
