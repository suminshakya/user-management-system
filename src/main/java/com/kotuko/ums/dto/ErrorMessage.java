package com.kotuko.ums.dto;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Builder
public class ErrorMessage {

    private String apiPath;
    private HttpStatus status;
    private List<String> errors;
}
