package com.kotuko.ums.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.kotuko.ums.annotation.UniqueEmail;
import com.kotuko.ums.annotation.UniqueUsername;
import com.kotuko.ums.formatter.CustomDateDeserializer;
import com.kotuko.ums.marker.OnCreate;
import com.kotuko.ums.marker.OnUpdate;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.PastOrPresent;
import lombok.*;


import java.time.LocalDate;

import static com.kotuko.ums.util.MessageConstraints.ValidationMessageConstant.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {


    private Long id;

    @NotNull(message = USERNAME_REQUIRED, groups = OnCreate.class)
    @NotBlank(message = USERNAME_BLANKED, groups = OnCreate.class)
    @UniqueUsername(groups = {OnCreate.class, OnUpdate.class})
    private String username;
    @NotNull(message = PASSWORD_REQUIRED, groups = OnCreate.class)
    @NotBlank(message = PASSWORD_BLANKED, groups = OnCreate.class)
    private String password;
    @NotNull(message = FIRST_REQUIRED, groups = OnCreate.class)
    @NotBlank(message = FIRST_BLANKED, groups = OnCreate.class)
    private String firstName;
    @NotNull(message = LAST_REQUIRED, groups = OnCreate.class)
    @NotBlank(message = LAST_BLANKED, groups = OnCreate.class)
    private String lastName;
    @NotNull(message = EMAIL, groups = OnCreate.class)
    @NotBlank(message = EMAIL_BLANKED, groups = OnCreate.class)
    @UniqueEmail(groups = {OnCreate.class, OnUpdate.class})
    private String email;
    @NotNull(message = DOB_REQUIRED, groups = OnCreate.class)
    @PastOrPresent(message = NOT_APPICABLE_DATE, groups = OnCreate.class)
    @JsonDeserialize(using = CustomDateDeserializer.class)
    private LocalDate dateOfBirth;
}
