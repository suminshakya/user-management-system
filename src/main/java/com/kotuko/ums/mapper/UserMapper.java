package com.kotuko.ums.mapper;

import com.kotuko.ums.dto.UserDTO;
import com.kotuko.ums.model.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper extends BaseMapper<User, UserDTO> {
}
