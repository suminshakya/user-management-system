package com.kotuko.ums.repo;

import com.kotuko.ums.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {

    Optional<User> findByUsername(String username);

    Optional<User> findByUsernameOrEmail(String username, String email);

    Page<User> findByFirstName(String firstName, Pageable pageable);

    Page<User> findByLastName(String lastName, Pageable pageable);

    Optional<User> findByEmail(String email);
}
