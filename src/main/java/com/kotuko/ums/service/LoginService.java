package com.kotuko.ums.service;

import com.kotuko.ums.dto.AuthUserDTO;

public interface LoginService {

    AuthUserDTO.Response authenticate(AuthUserDTO.LoginRequest userLogin);
}
