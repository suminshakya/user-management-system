package com.kotuko.ums.service;

import com.kotuko.ums.dto.UserDTO;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface UserService {

    UserDTO add(UserDTO user);

    UserDTO update(Long id, UserDTO user);

    UserDTO delete(Long id);

    UserDTO getByUsername(String username);

    UserDTO getByEmail(String email);

    List<UserDTO> getByFirstName(String firstName, Pageable pageable);

    List<UserDTO> getByLastName(String lastName, Pageable pageable);
}
