package com.kotuko.ums.service.impl;

import com.kotuko.ums.dto.AuthUser;
import com.kotuko.ums.repo.UserRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import static com.kotuko.ums.util.MessageConstraints.ErrorMessage.USER_NOT_FOUND;


@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserRepository userRepository;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
       return userRepository.findByUsername(username.trim()).map(AuthUser::new)
                .orElseThrow(() -> new UsernameNotFoundException(USER_NOT_FOUND));
    }


}
