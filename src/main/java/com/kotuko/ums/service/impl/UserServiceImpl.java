package com.kotuko.ums.service.impl;

import com.kotuko.ums.dto.UserDTO;
import com.kotuko.ums.exception.DataNotFoundException;
import com.kotuko.ums.mapper.UserMapper;
import com.kotuko.ums.model.User;
import com.kotuko.ums.repo.UserRepository;
import com.kotuko.ums.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.List;
import java.util.Objects;

import static com.kotuko.ums.util.MessageConstraints.ErrorMessage.*;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final UserMapper userMapper;
    private final PasswordEncoder passwordEncoder;

    @Override
    public UserDTO add(UserDTO userDTO) {
        User userEntity = userMapper.mapToEntity(userDTO);
        userEntity.setPassword(passwordEncoder.encode(userDTO.getPassword()));
        userRepository.save(userEntity);

        userDTO.setId(userEntity.getId());
        userDTO.setPassword(null);
        return userDTO;
    }

    @Override
    public UserDTO update(Long id, UserDTO userDTO) {
        User user = userRepository.findById(id).get();

        user.setUsername(Objects.isNull(userDTO.getUsername()) ? user.getUsername() : userDTO.getUsername());
        user.setFirstName(Objects.isNull(userDTO.getFirstName()) ? user.getFirstName() : userDTO.getFirstName());
        user.setLastName(Objects.isNull(userDTO.getLastName()) ? user.getLastName() : userDTO.getLastName());
        user.setEmail(Objects.isNull(userDTO.getEmail()) ? user.getEmail() : userDTO.getEmail());
        user.setDateOfBirth(Objects.isNull(userDTO.getDateOfBirth()) ? user.getDateOfBirth() : userDTO.getDateOfBirth());
        user.setPassword(Objects.isNull(userDTO.getPassword()) ? user.getPassword() : passwordEncoder.encode(userDTO.getPassword()));

        User updateduser = userRepository.save(user);
        return userMapper.mapToDTO(updateduser);
    }

    @Override
    public UserDTO delete(Long id) {
        User user = userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(USER_NOT_FOUND));
        userRepository.delete(user);
        return userMapper.mapToDTO(user);
    }


    @Override
    public UserDTO getByUsername(String username) {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new DataNotFoundException(USER_NOT_FOUND));
        return userMapper.mapToDTO(user);

    }

    @Override
    public UserDTO getByEmail(String email) {
        User user = userRepository.findByEmail(email).orElseThrow(() -> new DataNotFoundException(USER_NOT_FOUND));
        return userMapper.mapToDTO(user);
    }

    @Override
    public List<UserDTO> getByFirstName(String firstName, Pageable pageable) {
        List<User> users = userRepository.findByFirstName(firstName, pageable).toList();
        if (users.isEmpty()) {
            return Collections.emptyList();
        }
        return userMapper.mapToDTOs(users);
    }

    @Override
    public List<UserDTO> getByLastName(String lastName, Pageable pageable) {
        List<User> users = userRepository.findByLastName(lastName, pageable).toList();
        if (users.isEmpty()) {
            return Collections.emptyList();
        }
        return userMapper.mapToDTOs(users);
    }
}
