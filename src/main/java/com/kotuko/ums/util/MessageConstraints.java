package com.kotuko.ums.util;

public final class MessageConstraints {

    private MessageConstraints() {}

    public interface APIConstant{
        String URL_CONSTANT = "/api";
        String USERS = "/users";
        String FIRSTNAME = "/first-name";
        String LASTNAME = "/last-name";
        String EMAIL = "/email";
        String LOGIN = "/login";

        String ID_PATH = "/{id}";
        String FIRST_NAME_PATH = "/{firstName}";
        String LAST_NAME_PATH = "/{lastName}";
        String EMAIL_PATH = "/{email}";
        String USERNAME_PATH = "/{username}";

    }

    public interface ValidationMessageConstant{
        String USERNAME_REQUIRED = "Required: Username is required";
        String PASSWORD_REQUIRED = "Required: Password is required";
        String FIRST_REQUIRED = "Required: FirstName is required";
        String LAST_REQUIRED = "Required: LastName is required";
        String EMAIL = "Required: Email is required";
        String DOB_REQUIRED = "Required: Date of birth is required";
        String NOT_APPICABLE_DATE = "Date of birth must be in the past";

        String USERNAME_BLANKED = "Required: Username should not be blanked";
        String FIRST_BLANKED = "Required: FirstName should not be blanked";
        String LAST_BLANKED = "Required: LastName should not be blanked";
        String EMAIL_BLANKED = "Required: Email should not be blanked";
        String PASSWORD_BLANKED = "Required: Password should not be blanked";

        String INVALID_DATE ="Invalid date format. Expected format: yyyy-MM-dd";

    }

    public interface SuccessfulMessage{

        String USER_ADDED = "User has been added successfully";
        String USER_UPDATED = "User has been updated successfully";
        String USER_DELETED = "User has been deleted successfully";
        String USER_AUTHENTICATED = "User logged in successfully";
    }

    public interface ErrorMessage{

        String USER_NOT_FOUND = "User not found";

    }
}
