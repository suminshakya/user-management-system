package com.kotuko.ums.validator;

import com.kotuko.ums.annotation.UniqueEmail;
import com.kotuko.ums.exception.DataNotFoundException;
import com.kotuko.ums.model.User;
import com.kotuko.ums.repo.UserRepository;
import com.kotuko.ums.util.SecurityUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.AllArgsConstructor;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerMapping;

import java.util.Map;
import java.util.Optional;

import static com.kotuko.ums.util.MessageConstraints.ErrorMessage.USER_NOT_FOUND;


@AllArgsConstructor
public class UniqueEmailValidator implements ConstraintValidator<UniqueEmail, String> {

    private final UserRepository userRepository;

    @Override
    public boolean isValid(String email, ConstraintValidatorContext context) {
        if (email == null || email.isEmpty()) {
            return true;
        }
        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        final String loggedUser = SecurityUtil.getLoggedUser();
        Optional<User> emailExistedUser = userRepository.findByEmail(email.trim());

        if(!emailExistedUser.isEmpty() &&  request.getMethod().equals("PUT") ){
            Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
            Long id = Long.parseLong(pathVariables.get("id").toString());
            User userToBeUpdated = userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(USER_NOT_FOUND));

            if(emailExistedUser.get().getUsername().equals(userToBeUpdated.getUsername())){
                return true;
            }

            if (loggedUser.equals(emailExistedUser.get().getUsername())) {
                emailExistedUser.get();
            }
        }

        return emailExistedUser.isEmpty();
    }

}
