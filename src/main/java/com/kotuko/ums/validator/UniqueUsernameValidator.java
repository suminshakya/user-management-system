package com.kotuko.ums.validator;

import com.kotuko.ums.annotation.UniqueUsername;
import com.kotuko.ums.exception.DataNotFoundException;
import com.kotuko.ums.model.User;
import com.kotuko.ums.repo.UserRepository;
import com.kotuko.ums.util.SecurityUtil;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;
import lombok.AllArgsConstructor;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.servlet.HandlerMapping;

import java.util.Map;
import java.util.Optional;

import static com.kotuko.ums.util.MessageConstraints.ErrorMessage.USER_NOT_FOUND;


@AllArgsConstructor
public class UniqueUsernameValidator implements ConstraintValidator<UniqueUsername, String> {

    private final UserRepository userRepository;

    @Override
    public boolean isValid(String username, ConstraintValidatorContext context) {
        if (username == null || username.isEmpty()) {
            return true;
        }

        HttpServletRequest request = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();

        final String loggedUser = SecurityUtil.getLoggedUser();
        Optional<User> existedUser = userRepository.findByUsername(username.trim());

        if(!existedUser.isEmpty() &&  request.getMethod().equals("PUT") ){
            Map pathVariables = (Map) request.getAttribute(HandlerMapping.URI_TEMPLATE_VARIABLES_ATTRIBUTE);
            Long id = Long.parseLong(pathVariables.get("id").toString());
            User userToBeUpdated = userRepository.findById(id).orElseThrow(() -> new DataNotFoundException(USER_NOT_FOUND));

            if(existedUser.get().getUsername().equals(userToBeUpdated.getUsername())){
                return true;
            }

            if (loggedUser.equals(existedUser.get().getUsername())) {
                existedUser.get();
            }
        }

        return existedUser.isEmpty();
    }
}
