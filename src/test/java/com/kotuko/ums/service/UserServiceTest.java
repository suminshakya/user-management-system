package com.kotuko.ums.service;


import com.kotuko.ums.dto.UserDTO;
import com.kotuko.ums.exception.DataNotFoundException;
import com.kotuko.ums.mapper.UserMapper;
import com.kotuko.ums.model.User;
import com.kotuko.ums.repo.UserRepository;
import com.kotuko.ums.service.impl.UserServiceImpl;
import com.kotuko.ums.util.UserMockData;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.mockito.*;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Optional;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceTest {

    private UserDTO userDTO;
    private User user;
    private Pageable pageable;
    private Page<User> userPage;
    private ArgumentCaptor<User> userCaptor;

    @InjectMocks
    private UserServiceImpl userService;

    @Mock
    private UserRepository userRepository;

    @Mock
    private UserMapper userMapper;

    @Mock
    private PasswordEncoder passwordEncoder;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        userDTO = UserMockData.mockUserDTOData();
        user = UserMockData.mockUserData();
        pageable = PageRequest.of(0, 3);
        userPage = new PageImpl<>(Collections.singletonList(user), pageable, 1);
        userCaptor = ArgumentCaptor.forClass(User.class);
    }


    @Test
    void shouldAddUser() {

        Mockito.when(userMapper.mapToEntity(userDTO)).thenReturn(user);
        Mockito.when(passwordEncoder.encode(userDTO.getPassword())).thenReturn("user");
        UserDTO returnedUser = userService.add(userDTO);

        Mockito.verify(userRepository).save(userCaptor.capture());
        User capturedUser = userCaptor.getValue();

        Assertions.assertEquals(capturedUser.getUsername(), returnedUser.getUsername());
        Assertions.assertEquals(capturedUser.getEmail(), returnedUser.getEmail());
        Assertions.assertEquals(capturedUser.getFirstName(), returnedUser.getFirstName());
        Assertions.assertEquals(capturedUser.getLastName(), returnedUser.getLastName());
        Assertions.assertEquals(capturedUser.getDateOfBirth(), returnedUser.getDateOfBirth());
    }


    @Test
    void shouldUpdateUser() {
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        Mockito.when(passwordEncoder.encode(userDTO.getPassword())).thenReturn("user");
        Mockito.when(userMapper.mapToDTO(user)).thenReturn(userDTO);

        userService.update(1L, userDTO);
        Mockito.verify(userRepository).save(userCaptor.capture());
        User capturedUser = userCaptor.getValue();

        Assertions.assertNotNull(capturedUser);
    }


    @Test
    void shouldDeleteUser() {
        Mockito.when(userRepository.findById(1L)).thenReturn(Optional.of(user));
        Mockito.when(userMapper.mapToDTO(user)).thenReturn(userDTO);

        userService.delete(1L);

        Mockito.verify(userRepository, Mockito.times(1)).delete(user);
    }

    @Test
    void shouldThrowExceptionWhenDeleteUserNotFound() {
        Assertions.assertThrows(DataNotFoundException.class, () -> {
            userService.delete(1L);
        });
    }

    @Test
    void shouldRetrieveUserByFirstName() {
        Mockito.when(userRepository.findByFirstName(user.getFirstName(), pageable)).thenReturn(userPage);
        Mockito.when(userMapper.mapToDTOs(Arrays.asList(user))).thenReturn(Arrays.asList(userDTO));

        List<UserDTO> returnedUser = userService.getByFirstName(user.getFirstName(), pageable);
        UserDTO returnedUserDTO = returnedUser.get(0);

        Assertions.assertEquals(userDTO.getUsername(), returnedUserDTO.getUsername());
        Assertions.assertEquals(userDTO.getEmail(), returnedUserDTO.getEmail());
        Assertions.assertEquals(userDTO.getFirstName(), returnedUserDTO.getFirstName());
        Assertions.assertEquals(userDTO.getLastName(), returnedUserDTO.getLastName());
        Assertions.assertEquals(userDTO.getDateOfBirth(), returnedUserDTO.getDateOfBirth());
    }

    @Test
    void shouldReturnEmptyWhenRetrieveUserByFirstName() {
        userPage = new PageImpl<>(Collections.emptyList(), pageable, 1);
        Mockito.when(userRepository.findByFirstName(user.getFirstName(), pageable)).thenReturn(userPage);
        List<UserDTO> returnedUser = userService.getByFirstName(user.getFirstName(), pageable);

        Assertions.assertEquals(returnedUser.size(), 0);
    }

    @Test
    void shouldRetrieveUserByUsername() {
        Mockito.when(userRepository.findByUsername(user.getUsername())).thenReturn(Optional.ofNullable(user));
        Mockito.when(userMapper.mapToDTO(user)).thenReturn(userDTO);

        UserDTO returnedUser = userService.getByUsername(user.getUsername());
        Assertions.assertNotNull(returnedUser);
    }

    @Test
    void shouldThrowExceptionWhenRetrieveUserByUsernameNotFound() {
        Assertions.assertThrows(DataNotFoundException.class, () -> {
            userService.getByUsername(user.getUsername());
        });
    }


    @Test
    void shouldRetrieveUserByEmail() {
        Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(user));
        Mockito.when(userMapper.mapToDTO(user)).thenReturn(userDTO);

        UserDTO returnedUser = userService.getByEmail(user.getEmail());
        Assertions.assertNotNull(returnedUser);
    }

    @Test
    void shouldThrowExceptionWhenRetrieveUserByEmailNotFound() {
        Assertions.assertThrows(DataNotFoundException.class, () -> {
            userService.getByEmail(user.getEmail());
        });
    }


    @Test
    void shouldReturnEmptyWhenRetrieveUserByEmail() {
        userPage = new PageImpl<>(Collections.emptyList(), pageable, 1);
        Mockito.when(userRepository.findByEmail(user.getEmail())).thenReturn(Optional.ofNullable(user));
        Mockito.when(userMapper.mapToDTO(user)).thenReturn(userDTO);

        UserDTO returnedUser = userService.getByEmail(user.getEmail());
        Assertions.assertNotNull(returnedUser);

    }

    @Test
    void shouldRetrieveUserByLastName() {
        Mockito.when(userRepository.findByLastName(user.getLastName(), pageable)).thenReturn(userPage);
        Mockito.when(userMapper.mapToDTOs(Arrays.asList(user))).thenReturn(Arrays.asList(userDTO));

        List<UserDTO> returnedUser = userService.getByLastName(user.getLastName(), pageable);
        Assertions.assertEquals(returnedUser.size(), 1);
    }


    @Test
    void shouldReturnEmptyWhenRetrieveUserByLastName() {
        userPage = new PageImpl<>(Collections.emptyList(), pageable, 1);
        Mockito.when(userRepository.findByLastName(user.getLastName(), pageable)).thenReturn(userPage);

        List<UserDTO> returnedUser = userService.getByLastName(user.getLastName(), pageable);
        Assertions.assertEquals(returnedUser.size(), 0);

    }




}
