package com.kotuko.ums.util;

import com.kotuko.ums.dto.UserDTO;
import com.kotuko.ums.model.User;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

public class UserMockData {

    public static User mockUserData() {
        return User.builder().username("test").email("test@test.com")
                .firstName("testFirstName")
                .lastName("testLastname")
                .password("testPassword")
                .dateOfBirth(LocalDate.parse("2022-01-22", DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                .build();
    }

    public static UserDTO mockUserDTOData() {
        return UserDTO.builder()
                .username("test")
                .email("test@test.com")
                .firstName("testFirstName")
                .lastName("testLastname")
                .password("testPassword")
                .dateOfBirth(LocalDate.parse("2022-01-22", DateTimeFormatter.ofPattern("yyyy-MM-dd")))
                .build();
    }
}
